package com.zuitt;
import java.util.Scanner;
public class LeapYear {

    public static void main (String[] args){

        Scanner newScanner = new Scanner(System.in);
        System.out.println("Enter a valid year");
        int valid_year = newScanner.nextInt();

        if (valid_year % 400 == 0) {
            System.out.println(valid_year + " is a leap year.");
        } else if (valid_year % 100 == 0) {
            System.out.println(valid_year + " is not a leap year.");
        } else if (valid_year % 4 == 0) {
            System.out.println(valid_year + " is a leap year.");
        } else {
            System.out.println(valid_year + " is not a leap year.");
        }
    }
}
